import authentication from "./authentication";
import cors from "./cors";
import responses from "./responses";
import transformOrigin from "./transformOrigin";

export {
	authentication,
	cors,
	responses,
	transformOrigin,
};
