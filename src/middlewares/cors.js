import cors from "cors";

export default cors({
	origin: (origin, callback) => {
		const whiteList = ["https://stationf.co"];

		// Handle development case
		if (Config.NODE_ENV !== "production")
			return callback(null, true);

		// Handle case origin is listed in whitelist
		if (whiteList.includes(origin))
			return callback(null, true);

		// Handle other cases
		Log.error(`CORS blocked request from ${origin}`);
		return callback("You are not allowed to access this route.", false);
	},
});
