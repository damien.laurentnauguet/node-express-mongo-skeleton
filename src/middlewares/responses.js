import * as httpResponses from "../lib/responses";

/**
 * This middleware permits to use custom responses.
 */
export default (req, res, next) => {
	// Map http responses to res object
	for (const entry of Object.entries(httpResponses))
		res[entry[0]] = entry[1](res);

	next();
};
