/**
 * This middleware updates request headers with origin or host
 */
export default (req, res, next) => {
	req.headers.origin = req.headers.origin || req.headers.host;
	next();
};
