import mongoose from "mongoose";

// Connect to mongo database
const database = mongoose.connection;
const uri = `${Config.DATABASE_URI}${Config.DATABASE_NAME}`;
const connection = () => mongoose.connect(uri, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useCreateIndex: true,
	retryWrites: true,
	useUnifiedTopology: true,
});

database.on("error", () => mongoose.disconnect());
database.on("disconnected", () => Utils.sleep(5000, connection));

/**
 * To connect, replace this comment by: connection();
 */
