import jwt from "jsonwebtoken";

export default (payload, expiresIn) => (
	new Promise((resolve, reject) => (
		jwt.sign(payload, Config.JWT_SECRET, { expiresIn: parseInt(expiresIn, 10) * 1000 }, (error, token) => {
			if (error)
				reject(error);
			else
				resolve(token);
		})
	))
);
