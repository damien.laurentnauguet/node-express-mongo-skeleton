import jwt from "jsonwebtoken";

export default (token) => (
	new Promise((resolve, reject) => (
		jwt.verify(token, Config.JWT_SECRET, (error, payload) => {
			if (error)
				reject(error);
			else
				resolve(payload);
		})
	))
);
