export default (res) => (payload = {}, success = true) => res.status(200).json({ success, payload });
