export default (res) => (message = "An error occured.", success = false) => res.status(400).json({ success, message });
