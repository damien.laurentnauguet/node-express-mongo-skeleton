export default (res) => (message = "Forbidden access.", success = false) => res.status(403).json({ success, message });
