export default (res) => (message = "Unauthaurized access.", success = false) => res.status(403).json({ success, message });
