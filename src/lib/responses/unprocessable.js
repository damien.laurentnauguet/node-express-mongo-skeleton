export default (res) => (message = "An error occured.", success = false) => res.status(422).json({ success, message });
