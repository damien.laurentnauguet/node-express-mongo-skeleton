import badRequest from "./badRequest";
import forbidden from "./forbidden";
import success from "./success";
import unauthorized from "./unauthorized";
import unprocessable from "./unprocessable";

export {
	badRequest,
	forbidden,
	success,
	unauthorized,
	unprocessable,
};
