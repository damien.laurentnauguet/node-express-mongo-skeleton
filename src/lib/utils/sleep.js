export default (ms, callback) => {
	const wait = new Date().getTime() + ms;

	do {
		// eslint-disable-line
	} while (wait > new Date().getTime());

	return callback ? callback() : null;
};
