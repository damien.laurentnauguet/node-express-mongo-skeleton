export default (Model, data) => (
	new Promise((resolve, reject) => (
		Model.insertMany(data, (error, documents) => {
			if (error)
				reject(error);
			else
				resolve(documents);
		})
	))
);
