import create from "./create";
import deleteMany from "./deleteMany";
import find from "./find";
import findByIdAndDelete from "./findByIdAndDelete";
import findByIdAndUpdate from "./findByIdAndUpdate";
import findOne from "./findOne";
import insertMany from "./insertMany";
import populate from "./populate";
import pull from "./pull";

export {
	create,
	deleteMany,
	find,
	findByIdAndDelete,
	findByIdAndUpdate,
	findOne,
	insertMany,
	populate,
	pull,
};
