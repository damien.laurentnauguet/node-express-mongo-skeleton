/* eslint-disable no-process-env */

import winston from "winston";

import * as Controllers from "./controllers";
import * as Models from "./models";
import * as Migrations from "./migrations";
import * as JsonWebToken from "./lib/jsonwebtoken";
import * as Mongo from "./lib/mongo";
import * as Utils from "./lib/utils";

// Import environment variables
const env = process.env.NODE_ENV === "production" ? "production" : "development";
const { parsed } = require("dotenv").config({ path: `config/.env.${env}` });

// Setup general configuration variables
global.Config = {
	NODE_ENV: process.env.NODE_ENV || parsed.NODE_ENV || "development",
	CI: process.env.PORT || parsed.CI || false,
	PORT: process.env.PORT || parsed.PORT || 3000,
	MIGRATE: process.env.MIGRATE || parsed.MIGRATE || false,
	DATABASE_URI: parsed.DATABASE_URI,
	DATABASE_NAME: parsed.DATABASE_NAME,
};

// JSON web tokens
global.JsonWebToken = JsonWebToken;

// Utils
global.Utils = Utils;

// Database
global.Mongo = Mongo;
global.Migrations = Migrations;
global.Models = Models;

// Controllers
global.Controllers = Controllers;

// Logger
global.Log = winston.createLogger({
	transports: [
		new winston.transports.Console({
			level: "info",
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.simple(),
			),
		}),
	],
});
