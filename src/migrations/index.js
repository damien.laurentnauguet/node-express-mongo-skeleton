import * as data from "./data";
import * as Models from "../models";
import { deleteMany, insertMany } from "../lib/mongo";

export default {

	/**
	 * Helper to clean all models.
	 *
	 * @example
	 * deleteAll()
	 */
	deleteAll: async () => {
		try {
			await Promise.all(Object.values(Models).map((Model) => deleteMany(Model)));
			Log.info("[Migrations - deleteAll] Deletion successful");
		}
		catch (error) {
			Log.error(`[Migrations - deleteAll] ${error.message}`);
		}
	},

	/**
	 * Helper to migrate all models.
	 *
	 * @example
	 * migrate()
	 */
	migrate: async () => {
		try {
			await Promise.all(Object.values(data).map((migration) => insertMany(Models[migration.model], migration.data)));
			Log.info(`[Migrations - feed] Migrations of '${Object.keys(data).join(", ")}' successful`);
		}
		catch (error) {
			Log.error(`[Migrations - feed] ${error.message}`);
		}
	},
};
