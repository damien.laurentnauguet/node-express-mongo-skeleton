export default {
	handlerA: (req, res) => res.success({ message: "Hello A" }),
	handlerB: (req, res) => res.success({ message: "Hello B" }),
};
