/* eslint-disable no-process-env */
/* eslint-disable no-process-exit */

import http from "http";
import express from "express";

import * as Middlewares from "./middlewares";

require("./globals");
require("./mongo");

// Configure server
(async () => {
	try {
		const app = express();

		// Since express v4.16.0, express.json() replaces bodyparser.json()
		app.use(express.json());

		// Set host as origin if this latter is not defined
		app.use(Middlewares.transformOrigin);

		// Add custom responses
		app.use(Middlewares.responses);

		// Set CORS rules
		app.use(Middlewares.cors);

		// Set router
		app.get("/a", Controllers.someController.handlerA);
		app.get("/b", Controllers.someController.handlerB);

		// Run server
		await http.Server(app).listen(Config.PORT);

		// Exit if process run through CI
		if (Config.CI)
			process.exit();

		// Handle migrations
		if (["true", true].includes(Config.MIGRATE)) {
			await Migrations.deleteAll();
			await Migrations.migrate();
		}

		Log.info(`[Server] Listening on ${Config.PORT}`);
	}
	catch (error) {
		Log.error(error.message);
	}
})();
